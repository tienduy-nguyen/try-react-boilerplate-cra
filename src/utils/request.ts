export class ResponseError extends Error {
  public response: Response;
  constructor(response: Response) {
    super(response.statusText);
    this.response = response;
  }
}

function parseJSON(res: Response) {
  if (res.status === 204 || res.status === 205) {
    return null;
  }
  return res.json();
}

function checkStatus(res: Response) {
  if (res.status >= 200 && res.status < 300) {
    return res;
  }
  const error = new ResponseError(res);
  error.response = res;
  throw error;
}

export async function request(url: string, options?: RequestInit) {
  const fetchResponse = await fetch(url, options);
  const response = checkStatus(fetchResponse);
  return parseJSON(response);
}
