import 'whatwg-fetch';
import { request } from '../request';

declare let window: { fetch: jest.Mock };

describe('request', () => {
  beforeEach(() => {
    window.fetch = jest.fn();
  });
  describe('Testing with successful response', () => {
    beforeEach(() => {
      const res = new Response('{"hello": "world"}', {
        status: 200,
        headers: {
          'Content-type': 'application/json',
        },
      });
      window.fetch.mockReturnValue(Promise.resolve(res));
    });

    it('Should format the response correctly', done => {
      request('/correct-url')
        .catch(done)
        .then(json => {
          expect(json.hello).toBe('world');
          done();
        });
    });
  });

  describe('Testing with 204 response', () => {
    beforeEach(() => {
      const res = new Response('', {
        status: 204,
        statusText: 'Not content',
      });
      window.fetch.mockReturnValue(Promise.resolve(res));
    });
    it('Should return null on 204 response', async () => {
      const res = await request('/url');
      expect(res).toBeNull();
    });
  });

  describe('Testing with error response', () => {
    beforeEach(() => {
      const res = new Response('', {
        status: 404,
        statusText: 'Not found',
        headers: {
          'Content-type': 'application/json',
        },
      });
      window.fetch.mockReturnValue(Promise.resolve(res));
    });

    it('Should catch error', done => {
      request('/url-not-exists').catch(err => {
        expect(err.response.status).toBe(404);
        expect(err.response.statusText).toBe('Not found');
        done();
      });
    });
  });
});
