import { translations } from 'src/locales/translations';
import { _t } from 'src/utils/messages';

export const messages = {
  selectLanguage: () =>
    _t(
      translations.i18nFeature.selectLanguage,
      'Select Language', // default value
    ),
};
